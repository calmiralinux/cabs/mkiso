#!/usr/bin/bash -e
# Скрипт для создания LiveUSB образа Calmira GNU/Linux
# Форк утилиты из Olean Linux
# (C) 2021 Михаил Краснов <linuxoid85@gmail.com>

## SYNOPSIS
# mkiso.sh PATH_TO_DISTRO OUTPUT_ISO_IMAGE_FILE


##############
## Checking ##
##############

if [ $(id -u) != 0 ]; then
	echo "ERROR: $0 script need to run as root!"
	exit 1
fi

if [ "$(mksquashfs 2>&1 | grep "Xdict-size")" = "" ]; then
   echo "ERROR: mksquashfs (from 'squashfs-tools' package) not found or doesn't support xz compressing mode, aborting, no changes made"
   echo "You may consider installing squashfs-tools package"
   exit 1
fi

trap "exit 1" SIGHUP SIGINT SIGQUIT SIGTERM

##############################
## Default parameter values ##
##############################

CALMVERSION="2.0a1e3.2"

CWD=$PWD
WDIR=/tmp/calmiso
RAMDIR=/tmp/calmramfs
ISOLINUXDIR=$CWD/livecd/isolinux
DISTRONAME="Calmira GNU/Linux-libre"
LABEL="CALMIRA"
EFI_LABEL="EFIBOOTISO"
CALM_ROOT=$1

if [ -z "$2" ]; then
	OUTPUT="calmira-$CALMVERSION.iso"
else
	OUTPUT="$2"
fi

isolinux_files="chain.c32 isolinux.bin ldlinux.c32 libutil.c32 reboot.c32 menu.c32
isohdpfx.bin isolinux.cfg libcom32.c32 poweroff.c32"

####################################
## Reading the configuration file ##
####################################

# for file in "/etc/mkiso.conf" "~/.config/mkiso.conf"; do
# 	if [ -f "$file" ]; then
# 		source $file
# 	fi
# done
# unset file

# Preparing...

rm -rf     $WDIR
rm -rf     $RAMDIR
mkdir -pv {$WDIR,$RAMDIR}


# Prepare isolinux in working dir
mkdir -pv $WDIR/{filesystem,isolinux,boot/grub,EFI/BOOT}

for file in $isolinux_files; do
	cp $ISOLINUXDIR/$file $WDIR/isolinux
done

cp $CWD/livecd/virootfs/boot/grub/grub.cfg $WDIR/boot/grub/

# Заменить имя на отличное по умолчанию чтобы init правильно нашел диск
if [[ "$LABEL" != "CALMIRA" ]]; then
	sed -i "s/CALMIRA/$LABEL/g" $WDIR/isolinux/isolinux.cfg
	sed -i "s/CALMIRA/$LABEL/g" $WDIR/boot/grub/grub.cfg
fi

# Generate EFI image
grub-mkstandalone \
    -o ${WDIR}/EFI/BOOT/BOOTX64.EFI \
    -O x86_64-efi "boot/grub/grub.cfg=${WDIR}/boot/grub/grub.cfg"

dd if=/dev/zero of=${WDIR}/EFI/BOOT/efi.img bs=1M count=32
mkfs.msdos -F 12 -n "$EFI_LABEL" ${WDIR}/EFI/BOOT/efi.img
mmd -i ${WDIR}/EFI/BOOT/efi.img ::EFI
mmd -i ${WDIR}/EFI/BOOT/efi.img ::EFI/BOOT
mcopy -i ${WDIR}/EFI/BOOT/efi.img ${WDIR}/EFI/BOOT/BOOTX64.EFI ::EFI/BOOT/BOOTX64.EFI

echo "$DISTRONAME" > ${WDIR}/isolinux/venomlive

cp -v $CALM_ROOT/boot/vmlinuz-?.?-gnu $WDIR/boot/vmlinuz
cp -v $CALM_ROOT/boot/vmlinuz-?.? $WDIR/boot/vmlinuz-proprietary

# Создание и хранение root.sfs (сквоша системы) сделано в текущей
# директории на всякий случай, чтобы при изменении в файлах
# загрузчика (и прочих не относящихся к системе) не пересобирать
# всю систему в сквош, а скопировать готовый

# Создание файла 'calm-release' с инф-ей о системе
#cat > $CALM_ROOT/etc/calm-release << EOF
#[system]
#name = "Calmira GNU/Linux-libre"
#version = "$CALMVERSION"
#codename = "Coma Berenices"
#description = "Calmira $CALMVERSION GNU/Linux-libre"
#arch = "x86_64" # x86_64/x86
#build = "3"
#build_date = "$(date)" # installed automatically
#edition = "core" # core/extended

#[maintainer]
#name = "Michail Krasnov"
#email = "linuxoid85@gmail.com"
#years = "2021, 2022, 2023"

#[resources]
#organization = "https://gitlab.com/calmiralinux"
#master_repo = "https://gitlab.com/calmiralinux/calmiralinux"
#releases = "https://gitlab.com/calmiralinux/calmiralinux/releases"
#issues = "https://gitlab.com/calmiralinux/calmiralinux/issues"
#EOF

# Не генерировать файл если root.sfs уже есть
# иначе mksquashfs добавит копии папок.
if [[ ! -f "root.sfs" ]]; then
	mksquashfs $CALM_ROOT root.sfs           \
		-b 1048576 -comp xz -Xdict-size 100%   \
		-e $CALM_ROOT/tmp/*                    \
		-e $CALM_ROOT/usr/src/*
fi

cp root.sfs $WDIR/filesystem

# Prepare ramfs
mkdir -pv $RAMDIR/{bin,sbin,etc,proc,usr,usr/bin,usr/sbin,dev,sys,mnt,mnt/rootfs}
cp -aR $CWD/livecd/virootfs/bin $RAMDIR
cp -a $CWD/livecd/virootfs/init $RAMDIR
cp -a $CWD/livecd/virootfs/etc/fstab ${RAMDIR}/etc
cd $RAMDIR && find . | cpio -H newc -o | gzip -9 > $WDIR/boot/initrd.gz
cd $CWD

if [ -f "$OUTPUT" ]; then
	rm -rf $OUTPUT
fi

# Make iso
xorriso -as mkisofs                          \
		-r -J -joliet-long                       \
		-l -cache-inodes                         \
		-isohybrid-mbr $ISOLINUXDIR/isohdpfx.bin \
		-partition_offset 16                     \
		-volid "$LABEL"                          \
		-b isolinux/isolinux.bin                 \
		-c isolinux/boot.cat                     \
		-no-emul-boot                            \
		-boot-load-size 4                        \
		-boot-info-table                         \
		-eltorito-alt-boot                       \
		-e EFI/BOOT/efi.img                      \
		-no-emul-boot                            \
		-isohybrid-gpt-basdat                    \
		-o $OUTPUT                               \
		$WDIR

# Cleanup
rm -rvf $WDIR
rm -rvf $RAMDIR
